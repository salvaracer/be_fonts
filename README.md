# EXT:be_fonts

## What does this extension do?

Allows you to customize your backend fonts in the extension configuration. Sizing of fonts and google fonts in modules, pagetree and inner frame are available.

----------

![Settings of extension.](https://img1.picload.org/image/rpcawidl/demosettings.png)

----------

![Demo of what the extension does.](https://img1.picload.org/image/rpcawidi/demo.png)

----------

![You should allow your own backend for external loads.](https://picload.org/image/rpoidacl/adblocker.png)

### ↑ Known Issues

> Your adblocker might block some of the backend fonts, because some font's were loaded from google and some fonts are localy delivered with this extension.