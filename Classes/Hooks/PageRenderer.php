<?php
namespace SalvatoreEckel\BeFonts\Hooks;

/***************************************************************
 *  Copyright notice
 *  (c) 2016-2017 Salvatore Eckel <salvaracer@gmx.de>
 *  All rights reserved
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * Class/Function which adds the necessary ExtJS and pure JS and CSS stuff to the backend.
 *
 * @author Salvatore Eckel <salvaracer@gmx.de>
 * @package TYPO3
 * @subpackage tx_be_fonts
 */
class PageRenderer implements SingletonInterface
{
    /**
     * wrapper function called by hook (\TYPO3\CMS\Core\Page\PageRenderer->render-preProcess)
     *
     * @param array $parameters : An array of available parameters
     * @param \TYPO3\CMS\Core\Page\PageRenderer $pageRenderer : The parent object that triggered this hook
     *
     * @return void
     */
    public function addJSCSS($parameters, &$pageRenderer)
    {
        $extRelPath = ExtensionManagementUtility::extRelPath('be_fonts');

        $extensionConfiguration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['be_fonts']);
        $fontfamily = $extensionConfiguration['fontFamily'];

        if (!empty($fontfamily)) {
            switch ($fontfamily) {
                case 'Blokletters':
                    $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/font_blokletters.css');
                    break;
                case '2Dumb':
                    $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/font_2dumb.css');
                    break;
                case 'CaviarDreams':
                    $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/font_caviardreams.css');
                    break;
                case 'SEASRN':
                    $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/font_seasrn.css');
                    break;
                case 'Baloo':
                    $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/font_baloo.css');
                    break;
                case 'Lora':
                    $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/font_lora.css');
                    break;
                case 'Karla':
                    $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/font_karla.css');
                    break;
                case 'Neuton':
                    $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/font_neuton.css');
                    break;
                case 'Scada':
                    $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/font_scada.css');
                    break;
                case 'Space Mono':
                    $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/font_spacemono.css');
                    break;
                default:
                    break;
            }
        }

        if ($extensionConfiguration['fontFamily'] != 'Default') {
            $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/backend.css');
            if (!empty($extensionConfiguration['useFontBeInner'])) {
                $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/backend_inner.css');
            }
        }
        if ($extensionConfiguration['fontSize'] != 'Default') {
            switch ($extensionConfiguration['fontSize']) {
                case 'Large':
                    $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/size_large.css');
                    break;
                case 'Huge':
                    $pageRenderer->addCssFile($extRelPath . 'Resources/Public/Css/size_huge.css');
                    break;
            }
        }
        # jquery ready file
        # $pageRenderer->addJsFile($extRelPath . 'Resources/Public/Js/backend.js');
    }

}
