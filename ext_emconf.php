<?php

# Extension Manager/Repository config file for ext "be_fonts"
$EM_CONF[$_EXTKEY] = array (
  'title' => 'TYPO3 Backend Fonts',
  'description' => 'Allows you to customize your backend fonts in the extension configuration. Sizing of fonts, google fonts in modules, pagetree and inner frame are available.',
  'category' => 'backend',
  'author' => 'Salvatore Eckel',
  'author_email' => 'salvaracer@gmx.de',
  'author_company' => 'infoeckel',
  'state' => 'stable',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearCacheOnLoad' => true,
  'version' => '8.7.3',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '7.6.0-8.99.999',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);
